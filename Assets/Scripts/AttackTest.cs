﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AttackTest : MonoBehaviour
{
    [SerializeField] private ParticleSystem m_Slash_A;
    [SerializeField] private ParticleSystem m_Slash_B;
    [SerializeField] private List<InputAction> m_TestActions = new List<InputAction>();

    private void Update()
    {
        foreach (var action in m_TestActions)
        {
            if (Input.GetKeyDown(action.ActionKey))
            {
                action.OnPerformAction?.Invoke();
            }
        }
    }

    public void PlaySlashA()
    {
        m_Slash_A.Play();
    }

    public void PlaySlashB()
    {
        m_Slash_B.Play();
    }
}


[System.Serializable]
public struct InputAction
{
    public string Name;
    public KeyCode ActionKey;
    public UnityEvent OnPerformAction;
}
